// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCA9CQxyPEoeC9tkRTh6z1_askDuiLfMko",
    authDomain: "angular-test-a.firebaseapp.com",
    databaseURL: "https://angular-test-a.firebaseio.com",
    projectId: "angular-test-a",
    storageBucket: "angular-test-a.appspot.com",
    messagingSenderId: "601958924101",
    appId: "1:601958924101:web:2785b56fac09bcc900082c",
    measurementId: "G-PW4QRH2YWH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
