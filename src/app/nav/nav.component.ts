import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})

export class NavComponent implements OnInit {

  collapsed = true;
  isAuthenticated = false;
  
  constructor(private authService: AuthService) { }

  ngOnInit() {
     this.authService.user.subscribe(user => {
      this.isAuthenticated = !!user;
      //this.isAuthenticated = !user ? false: true;
    });
  }

  onLogout(){
    this.authService.Logout();
    this.authService.userLogout.next(false);
  }

}
