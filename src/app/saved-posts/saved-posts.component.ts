import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  getPersonalPosts(userId: number){
    fetch('https://jsonplaceholder.typicode.com/posts') //need to add user id and my firebase url
    .then(response => response.json())
    .then(json => console.log(json)) //i get an object of posts[]
  }
}
