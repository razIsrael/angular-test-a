import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { TestComponent } from './test/test.component';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';
import { AuthGuardGuard } from './auth-guard.guard';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';



const appRoutes: Routes=[
    { path: '',   redirectTo: '/login', pathMatch: 'full' },
    {path: 'login', component: LoginComponent},
    {path: 'test', component: TestComponent, canActivate: [AuthGuardGuard]},
    {path: 'blog-posts', component: BlogPostsComponent},
    {path: 'blog-posts/:id', component: SavedPostsComponent},
    {path: 'saved-posts', component: SavedPostsComponent},
    {path: 'classified', component: ClassifiedComponent},
    {path: 'docform', component: DocformComponent},

    { path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
       ];
  
  @NgModule({
      imports: [
          RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
          )
        ],
        exports: [
          RouterModule
        ]
  })
  export class appRoutingModule {
  
  }