import { Component, OnInit } from '@angular/core';

import { AuthService, AuthResponseData } from '../auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLoginMode = true;
  isLoading = false;
  isUserInApp = false;
  //error: string = null;
  email: string = null;
  

  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.authService.userLogout.subscribe(data => {
      console.log('this is the data: ' + data);
      this.isUserInApp = data;
    });
  }

  onSwitchMode(){
    this.isLoginMode = !this.isLoginMode;
  }
   
  onSubmit(form: NgForm){
    //  console.log('im in on submit');
    if (!form.valid){
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    this.email = email;
    // console.log(`email: ${email} password: ${password}`);
    let authObs: Observable<AuthResponseData>;

    this.isLoading = true;
    if (this.isLoginMode) {
      this.authService.login(email,password);
    } else{
      this.authService.SignUp(email,password);
    }
    // console.log('befor error is ok');
    // authObs.subscribe( resData => {
    //   console.log(resData);
    //   this.isLoading = false;
    //   this.router.navigate(['/test']);
    // },
    // errorMessage => {
    //   console.log(errorMessage);
     
    //   this.error = errorMessage;
    //   this.isLoading = false;
    // });
    this.isUserInApp = true;

   this.isLoading = false;//added
    form.reset();
  }




}
