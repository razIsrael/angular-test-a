import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



export interface Posts {
  "userId": number,
  "id":number,
  "title": string,
  "body":   string
}


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  allPosts: Posts[] =[];

  constructor(private http: HttpClient) { }

  private URL = "https://jsonplaceholder.typicode.com/posts";//posts url

   getAllPosts(){
     
    this.allPosts = fetch('https://jsonplaceholder.typicode.com/posts') //need to add user id and my firebase url
    .then(response => response.json())
    .then(json => console.log(json)) //i get an object of posts[]
    return this.allPosts;
  }





// private transformPostsData(data:WeatherRaw):Posts {
//     return {
//       name:data.name,
//       country:data.sys.country,
//       image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
//       description:data.weather[0].description,
//       temperature: data.main.temp,
//       lat: data.coord.lat,
//       lon: data.coord.lon
//     }
//   }


}
