import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { environment } from '../environments/environment';

import {MatCardModule} from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';



import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { appRoutingModule } from './app-routing.module';
import { TestComponent } from './test/test.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    TestComponent,
    LoadingSpinnerComponent,
    ClassifiedComponent,
    DocformComponent,
    BlogPostsComponent,
    SavedPostsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    
    MatSliderModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,

    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    appRoutingModule,
    MatCardModule,

    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule    
  ],
  providers: [AngularFirestore,
    AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
