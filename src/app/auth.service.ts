import { Router } from '@angular/router';
import { Observable, throwError, Subject } from 'rxjs';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

import { User } from './User';
import { HttpErrorResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';


export interface AuthResponseData {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registerd?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  @Output() userLogout = new Subject<boolean>();

  user: Observable<User | null>;
  isUserAuthenticated = false;
  
  constructor(public afAuth: AngularFireAuth,
              private router: Router) {
    this.user = this.afAuth.authState;
  }

  getUser(){//need to call this
    return this.user
  }

  SignUp(email: string, password: string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res => 
          {
            console.log('Succesful sign up',res);
            this.isUserAuthenticated = true;
            this.router.navigate(['/login']);
          }
        ).catch(error => {
          // console.log(error.message);
          // this.handleError(error);
          alert(error.message);
        }

        );

  }

  Logout(){
    this.afAuth.auth.signOut();
    this.isUserAuthenticated = false;
     
  }

  login(email:string, password:string){
    console.log('im in login');
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.isUserAuthenticated = true;
              this.router.navigate(['/login']);
            }     
        ).catch(error => {
          console.log('im caching an error');
          console.log(error.message);
          //this.handleError(error);
          alert(error.message);
        }

        );
  }

  private handleError(errorRes: HttpErrorResponse){
    let errorMessage = 'An unknown error occured!';
    if (!errorRes.error || !errorRes.error.error){
      alert(errorMessage);
    }
    switch (errorRes.error.error.message) {
        case 'EMAIL_EXISTS':
            errorMessage = 'this email exists already';
            break;
        case 'EMAIL_NOT_FOUND':
            errorMessage = 'this email dos not exist';
            break;
        case 'INVALID_EMAIL':
            errorMessage = 'this is an invalid email';
            break;
        case 'INVALID_PASSWORD':
            errorMessage = 'this password is not correct';
            break;
            
      }
      alert(errorMessage);
}

isAuthenticated() {
  return this.isUserAuthenticated;
}



}
